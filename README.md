# libCloudSync

## What it is

A simple cpp interface to interact with remote cloud storage of a variety of providers.

This repository has been migrated [to Github](https://github.com/jothepro/libCloudSync). It now only hosts the Conan artifacts built in github.

## Installation

1. Add the repository as conan remote:
    ```
    conan remote add libcloudsync-gitlab https://gitlab.com/api/v4/projects/15425736/packages/conan
    ```
2. Add the library as dependency in your conanfile.
   ```
   [requires]
   libcloudsync/0.1.0@jothepro/release
   ```